import React from "react";
import ReactDom from "react-dom";
import "./qcm.css";

const data = 
{
  "response_code": 0,
  "results":[
    {
      "category": "General Knowledge",
      "type": "multiple",
      "difficulty": "easy",
      "question": "On a dartboard, what number is directly opposite No. 1?",
      "correct_answer": "19",
      "incorrect_answers": [
        "20",
        "12",
        "15"
      ]
    },
    {
      "category": "General Knowledge",
      "type": "multiple",
      "difficulty": "easy",
      "question": "Which American president appears on a one dollar bill?",
      "correct_answer": "George Washington",
      "incorrect_answers": [
        "Thomas Jefferson",
        "Abraham Lincoln",
        "Benjamin Franklin"
      ]
    },
    {
      "category": "General Knowledge",
      "type": "multiple",
      "difficulty": "easy",
      "question": "How many colors are there in a rainbow?",
      "correct_answer": "7",
      "incorrect_answers": [
        "8",
        "9",
        "10"
      ]
    },
    {
      "category": "General Knowledge",
      "type": "multiple",
      "difficulty": "easy",
      "question": "Which of these colours is NOT featured in the logo for Google?",
      "correct_answer": "Pink",
      "incorrect_answers": [
        "Yellow",
        "Blue",
        "Green"
      ]
    },
    {
      "category": "General Knowledge",
      "type": "multiple",
      "difficulty": "easy",
      "question": "Which one of these is not a typical European sword design?",
      "correct_answer": "Scimitar",
      "incorrect_answers": [
        "Falchion",
        "Ulfberht",
        "Flamberge"
      ]
    },
    {
      "category": "General Knowledge",
      "type": "multiple",
      "difficulty": "easy",
      "question": "What do the letters of the fast food chain KFC stand for?",
      "correct_answer": "Kentucky Fried Chicken",
      "incorrect_answers": [
        "Kentucky Fresh Cheese",
        "Kibbled Freaky Cow",
        "Kiwi Food Cut"
      ]
    },
    {
      "category": "General Knowledge",
      "type": "multiple",
      "difficulty": "easy",
      "question": "Who is the author of Jurrasic Park?",
      "correct_answer": "Michael Crichton",
      "incorrect_answers": [
        "Peter Benchley",
        "Chuck Paluhniuk",
        "Irvine Welsh"
      ]
    },
    {
      "category": "General Knowledge",
      "type": "multiple",
      "difficulty": "easy",
      "question": "Which of the following is not the host of a program on NPR?",
      "correct_answer": "Ben Shapiro",
      "incorrect_answers": [
        "Terry Gross",
        "Ira Glass",
        "Peter Sagal"
      ]
    },
    {
      "category": "General Knowledge",
      "type": "multiple",
      "difficulty": "easy",
      "question": "Which of the following is not an Ivy League University?",
      "correct_answer": "Stanford",
      "incorrect_answers": [
        "University of Pennsylvania",
        "Harvard",
        "Princeton"
      ]
    },
    {
      "category": "General Knowledge",
      "type": "multiple",
      "difficulty": "easy",
      "question": "When was the Playstation 3 released?",
      "correct_answer": "November 11, 2006",
      "incorrect_answers": [
        "January 8, 2007",
        "December 25, 2007",
        "July 16, 2006"
      ]
    }
  ]
}

// url = https://opentdb.com/api.php?amount=10&category=9&difficulty=easy&type=multiple

function Number(props){
	const number = props.number + 1;
	function nextQ(){
		props.next();
	}
	return(
		<div>
			<div id="page">{number}/{data.results.length}</div>
		</div>
	)

}

function Questions(props){
	const question = props.question;
	function nextQ(){
		props.next();
	}
	return (
		<h3>
			{question}
		</h3>
	)
}

function Reponses(props){
	function nextQ(){
		props.next();
	}
	function estCoche(e){
		if(e.target.checked){
			props.stockReponse(e.target.value);
		}
	}
	const reponseTrue = props.reponseTrue;
	const reponsesFalse = props.reponsesFalse;
	const propositions = [reponseTrue, ...reponsesFalse];
	const reponses = propositions.sort(() => Math.random() - 0.5);
		return (
		<div>
			<div>
				<input type="radio" name="qcm" value={reponses[0]} onChange={estCoche}/> 
				<label htmlFor="proposition1">{reponses[0]}</label>
			</div>
			<div>
				<input type="radio" name="qcm" value={reponses[1]} onChange={estCoche}/> 
				<label htmlFor="proposition2">{reponses[1]}</label>
			</div>
			<div>
				<input type="radio" name="qcm" value={reponses[2]} onChange={estCoche}/> 
				<label htmlFor="proposition3">{reponses[2]}</label>
			</div>
			<div>
				<input type="radio" name="qcm" value={reponses[3]} onChange={estCoche}/> 
				<label htmlFor="proposition4">{reponses[3]}</label>
			</div>	
		</div>
	)
}

function Button(props){
	function nextQ(){
		props.next();
		props.counter();
	}	
	return (
		<button onClick={nextQ}>VALIDER</button>
	)
}

function Score(props){
  const question = props.question;
  const number = props.index;
  const reponseTrue = props.reponseTrue;
  let reponseActuelle = "";

  function stockReponse(rep){
    reponseActuelle = rep;
  }
  
  if(number == data.results.length){
    return (
            <div id="score">
              Score : {count}/{data.results.length}
            </div>
        ) 
  }else{
    function nextQ(){
    props.next();
  }
    return (
            <div>
             Score : {count}/{data.results.length}
            </div>
      )
  }
}

let count = 0;

function Affichage(props){
  const number = props.index;
	const [tab, setTab] = React.useState(props.post);
	const [index, setIndex] = React.useState(0);
	let reponseActuelle = "";

  	function stockReponse(rep){
  		reponseActuelle = rep;
  	}

  	function nextQ(){
      if(index == data.results.length){
          Score();
      }else{
        setIndex(index + 1);
      }
  	}

  	function counter(){
  		if(reponseActuelle == props.post[index].correct_answer){
  			count++;
  		}
  	}
  
    if(index == data.results.length){
      return (
        <div>
        <Score next={nextQ} index={index} />
        </div>
        )
    }else {
  	return (
  	<div id="Tout">
  		<Number next={nextQ} number={index} />
  		<Questions next={nextQ} question={tab[index].question} />
  		<Reponses next={nextQ} stockReponse={stockReponse} reponseTrue={tab[index].correct_answer} reponsesFalse={tab[index].incorrect_answers} />
  		<Button next={nextQ} counter={counter} />
      <Score next={nextQ} index={index} question={tab[index].question} reponseTrue={tab[index].correct_answer} stockReponse={stockReponse}/>
  	</div>
  	)
  }
}

ReactDom.render(	
    <Affichage post={data.results} />,
    document.getElementById('app')
)